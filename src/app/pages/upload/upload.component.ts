import {Component, OnInit} from '@angular/core';
import {AddMessageMessage} from '../../classes/add-message-message';
import {MessageBackendService} from '../../services/message-backend.service';
import {Subject} from 'rxjs/Subject';
import {debounceTime} from 'rxjs/operator/debounceTime';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
    currentMessage: AddMessageMessage = {
        name: '',
        message: ''
    };
    currentFile;
    successMessage: string;
    uploadingMessage : string;
    private _success = new Subject<string>();

    constructor(private messageBackendService: MessageBackendService) {
    }

    ngOnInit() {
        this.currentMessage.name = localStorage.getItem('partyscreen-username');

        this._success.subscribe((message) => this.successMessage = message);
        debounceTime.call(this._success, 5000).subscribe(() => this.successMessage = null);
    }

    updateFile($event) {
        const files = $event.target.files;
        if (!files.length) {
            delete this.currentMessage.image;
        }
        const reader = new FileReader();
        reader.onload = (event: any) => {
            this.currentMessage.image = {
                base64: event.target.result
            };
        };
        reader.readAsDataURL(files[0]);
    }

    submit() {
        localStorage.setItem('partyscreen-username', this.currentMessage.name);
        this.uploadingMessage = 'Uploading.... please wait...';
        this.messageBackendService.addMessage(this.currentMessage).subscribe(() => {
            this.uploadingMessage = null;
            this.changeSuccessMessage('Deine Nachricht wurde versendet.');
            this.currentMessage.message = '';
            this.currentMessage.image.base64 = null;
            this.currentFile = null;
        });
    }

    public changeSuccessMessage(message: string) {
        this._success.next(message);
    }

}
