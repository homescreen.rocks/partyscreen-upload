import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {MainNavComponent} from './main-nav/main-nav.component';

import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {UploadComponent} from './pages/upload/upload.component';
import {MessagesComponent} from './pages/messages/messages.component';
import {MessageBackendService} from './services/message-backend.service';

const appRoutes: Routes = [
    {
        path: 'upload',
        component: UploadComponent,
        data: {title: 'Upload'}
    },
    {
        path: 'messages',
        component: MessagesComponent,
        data: {title: 'Messages'}
    },
    {
        path: '',
        redirectTo: '/upload',
        pathMatch: 'full'
    },
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    declarations: [
        AppComponent,
        MainNavComponent,
        PageNotFoundComponent,
        UploadComponent,
        MessagesComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        NgbModule.forRoot(),
        RouterModule.forRoot(
            appRoutes,
            {
                enableTracing: true, // <-- debugging purposes only
                useHash: true
            }
        )
    ],
    providers: [
        MessageBackendService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
