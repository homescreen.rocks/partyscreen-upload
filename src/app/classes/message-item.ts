import {ImageSource} from './image-source';

export class MessageItem {
    id: string;
    name: string;
    date: Date;
    message: string;
    sourceUrl: string;
    'user-id': string;
}
