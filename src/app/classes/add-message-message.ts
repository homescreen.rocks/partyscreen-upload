import {Image} from './image';

export class AddMessageMessage {
    public name: string;
    public message: string;
    public image?: Image;
}
