export class ImageSource {
    type: string;
    url: string;
}
